function sortArrSmall(num) {
    var done = false;
    while (!done) {
        done = true;
        for (var i = 1; i < num.length; i++) {
            if (num[i - 1] > num[i]) {
                done = false;
                var tmp = num[i - 1];
                num[i - 1] = num[i];
                num[i] = tmp;
            }
        }
    }
    return num;
}

function sortArrLarge(num) {
    var done = false;
    while (!done) {
        done = true;
        for (var i = 1; i < num.length; i += 1) {
            if (num[i - 1] < num[i]) {
                done = false;
                var tmp = num[i - 1];
                num[i - 1] = num[i];
                num[i] = tmp;
            }
        }
    }
    return num;
}

let num = [2, 4, 7, 1, 5, 13, -1, -10, -20];

console.log(sortArrSmall(num));
console.log(sortArrLarge(num));
